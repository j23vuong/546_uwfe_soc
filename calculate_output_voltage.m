function v_o = calculate_output_voltage(v_i, I, dt)
    persistent integ_I;
    persistent integ_VC;

    R_p = 0.0015;
    R_o = 0.0031;
    C = 9.6463e3;

    if isempty(integ_I)
        integ_I = 0;
        integ_VC = 0;
    end

    v_1 = I .* R_o;
    v_2 = (1/C) .* (integ_I - 1/R_p.*integ_VC);
    integ_VC = integ_VC + v_2 * dt;
    integ_I = integ_I + I * dt;
    v_o = v_i + v_1 + v_2;
end