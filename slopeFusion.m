function v_o = calculate_output_voltage(v_i, I, dt)
    persistent integ_I;
    persistent integ_VC;

    R_p = 0.0015;
    R_o = 0.0031;
    C = 9.6463e3;

    if isempty(integ_I)
        integ_I = 0;
        integ_VC = 0;
    end

    v_1 = I .* R_o;
    v_2 = (1/C) .* (integ_I - 1/R_p.*integ_VC);
    integ_VC = integ_VC + v_2 * dt;
    integ_I = integ_I + I * dt;
    v_o = v_i + v_1 + v_2;
end

latestDischarge = readtable('fullSocDischarge.csv');
%latestDischarge = readtable('socVariableLoad.csv');
latestDischarge = table2array(latestDischarge);

data = latestDischarge;

time = double(data(:, [1]));
time = time(2:end);
timeDur = time(end) - time(1);
percentTime = 100*(time-time(1))/timeDur;

current = double(data(:, [22]));
current = current(2:end);

voltage1 = double(data(:, [2]));
voltage1 = double(voltage1(2:end));

adjVoltage1 = zeros(length(time), 1);
cCount = zeros(length(time), 1);
adjVoltage1(1) = voltage1(1);
lastTime = time(1);
for i=2:length(time)
    adjVoltage1(i) = calculate_output_voltage(voltage1(i), current(i), (time(i) - lastTime)/1000);
    cCount(i) = cCount(i-1) + (current(i) * (time(i) - lastTime)/1000);
    lastTime = time(i);
end

%{
clf;
hold on;
plot(time, adjVoltage1);
plot(time, voltage1);
xlabel("Time [s]");
ylabel("Voltage [V]");
legend("Battery Model Cell Voltage", "Measured Cell Voltage");
title("Battery Model Performance over Full Discharge");
hold off;
%}

load("voltageToDischarge.mat");
V_soc = f(adjVoltage1);

lastSoc = 0;
soc = [];
load("voltageWeightFn.mat");

syms X;
weights = [];
for idx=1:length(percentTime)
    xWeights = subs(dEqn,X,lastSoc);
    weight = norm(double(2*((xWeights-0.0025) .^ (1/3)) + 0.3))
    if (weight > 1)
        weight = 1;
    end
    weights = [weights; weight];
    lastSoc = (weight*V_soc(idx)) + ((1-weight)*100*cCount(idx)/64800)
    soc = [soc; norm(lastSoc)];
end

clf;
hold on;
title("Test Data Slope Based Weighting Function SoC Estimate")
xlabel("Time [s]");
ylabel("SoC [%]");
plot(time, soc);
plot(time, V_soc);
plot(time, 100*(cCount/64800));
hold off;
