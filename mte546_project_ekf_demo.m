close all;
%% MTE546 Project EKF Testing

% You need to run IZ_sims with trials = 1 before this to get the current
% profile into the workspace

% voltage to SOC map
voltage_list = linspace(2.4, 4.1, 1000)
soc_list = 6077.86-19345.545*voltage_list+25830.306*voltage_list.^2-19107.323*voltage_list.^3+8622.4076*voltage_list.^4-2441.25816*voltage_list.^5+424.773203*voltage_list.^6-41.61711664*voltage_list.^7+1.7606327*voltage_list.^8;
soc = 0.3
idx = find(min(abs(soc_list - soc))==abs(soc_list - soc))

%% Initial data processing
data_10A = readmatrix('10ADischarge.csv');
times = data_10A(:, 1) - data_10A(1, 1);
voltages = data_10A(:, 2);

%% EKF

% Defined in lab report
delta_t = 0.05;

% Battery constants
total_capacity_coulombs = 6 * 3 * 3600;
current = -30; % update this depending on test data
Rp = 0.0015 % Ohms
Ro = 0.0031 % Ohms
C = 9646.3 % F

% Motion model (updated)
% A = [1 0; 
%     0 exp(-delta_t / (Rp * C))];

% Model noise covariance
Q = [0.005 0;
    0 0.8];

% Sensor model noise covariance
R = [0.000349271^2];

% Initial covariance
P = ones(2, 2);

% Initial state (updated but random)
x = [0.85; 3.9];

% Simulation stuff
voltage_readings = zeros(10, 2)
voltage_readings(:, 1) = 2.64*exp(-0.03241*0.3);
voltage_readings(:, 2) = 3.484*exp(-0.02183*0.3);

voltage_est = []
soc_est = []
voltage_actual = []

% Kalman filter iterations
arr_size = size(voltages)
arr_size = arr_size(1)
v2_integrated = 0;
i_integrated = 0;

for iteration = 2:arr_size
    current = -1 * profile(iteration)
    delta_t = (times(iteration) - times(iteration - 1)) / 1000

    % Prediction step
    % Apply circuit model
    soc_hat = x(1) + (current * delta_t / total_capacity_coulombs);
    soc = x(1)
    idx = find(min(abs(soc_list - soc))==abs(soc_list - soc))
    voc_hat = voltage_list(idx) 

    x_hat = [soc_hat; voc_hat];

    % State transition matrix (depends on time step, which varies)
    A = [1 0; 
        0 exp(-delta_t / (Rp * C))];

    P = A*P*transpose(A) + Q

    % Correction step
    % We want the voltage used in the model to be the no-load voltage, not
    % the raw reading, so it plays nicely with the voltage_list map
    v_raw = voltages(iteration);
    v2 = (1/C)*i_integrated - (1/(Rp*C)) * v2_integrated;

    % Update integral tracking
    v2_integrated = v2_integrated + (v2 * delta_t);
    i_integrated = i_integrated + (current * delta_t);

    v_scaled = v_raw - current * Ro - v2; 

    y_hat = v_scaled - x_hat(2);

    % sensor_model = [y_hat 0];

    % Need to generate a new jacobian based on the previous state
    % each time
    H = [diff(voltage_list(idx-1:idx)) 1]

    % This is just from wikipedia, idk if it will fully work
    S = H*P*transpose(H) + R;
    K = P*transpose(H)*inv(S);
    x = x_hat + K*y_hat;
    P = (eye(2) - K*H)*P;

    voltage_est = [voltage_est; x(2)];
    soc_est = [soc_est; x(1)];
    voltage_actual = [voltage_actual;  v_raw];
end

%% Plotting

figure();

subplot(3, 1, 1)
hold on;
title("EKF Results, 10 A Cell Discharge")
plot(times(2:end) / 1000, voltage_est)
plot(times / 1000, voltages)
legend(["No-Load Voltage Estimate", "Raw Voltage"])
ylabel("Voltage [V]")

subplot(3, 1, 2)
plot(times(2:end) / 1000, soc_est)
legend(["SoC Estimate"])
ylabel("SoC [%]")

subplot(3, 1, 3)
plot(times / 1000, profile(1:2070))
legend(["Current"])
ylabel("Current [A]")
xlabel("Time [s]")
