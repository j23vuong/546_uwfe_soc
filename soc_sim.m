polynomial_coeffs = [1.76063275065311	-41.6171166467157	424.773203034036	-2441.25816393788	8622.40759658763	-19107.3233169624	25830.3060680661	-19345.5446537532	6077.85580095108];
v_data = readmatrix('UWFEAdjVoltage.csv');
raw_data = v_data;

plot_raw = 0;
plot_adjusted = 0;
initial_time_ms = v_data(1,1);
for i = 2:length(v_data(1:end,1))
    if v_data(i,1) < v_data(i-1,1)
        v_data(i:end,1) = v_data(i:end,1) - v_data(i,1) + v_data(i-1,1) + 75;
    end
end
v_data(1:end, 1) = (v_data(1:end, 1) - initial_time_ms) / 1000;
v_data(232117:end, :) = [];
if plot_raw == 1
    figure()
    plot(v_data(1:end, 1), v_data(1:end, 2))
end

integ_VC = 0;
integ_I = 0;
v_o = [];
profile = [];
steady = linspace(0,0, 1);
load = linspace(3.6,3.6,length(v_data(1:end,1)));
profile = cat(2, steady, load);

voltage_soc = [];
current_soc = [];
cell_i = 3;

initial_v_soc = min(max(polyval(polynomial_coeffs, v_data(1, cell_i)), 0.0), 1.0);

charge_coulombs = 3*6*3600;

syms z
w_pos = [0.95 0.7 0.3 0.05];
z_pos = [0.75 0.25 0.75];
%blending_f = piecewise(z > w_pos(1), z_pos(1), z < w_pos(4), z_pos(3), w_pos(1) > z > w_pos(2), (z_pos(2) - z_pos(1)) / (w_pos(2) - w_pos(1)) * (z - w_pos(1)) + z_pos(1), w_pos(2) > z > w_pos(3), z_pos(2), w_pos(3) > z > w_pos(4), (z_pos(3) - z_pos(2)) / (w_pos(4) - w_pos(3)) * (z - w_pos(3)) + z_pos(2));
weighted_soc = [];
%figure()
%fplot(blending_f, [0 1])
integ_I = 0;

for i = 1:(size(v_data,1))-1
    dt = v_data(i+1,1) - v_data(i,1);
    v_o(i) = calculate_output_voltage(v_data(i,cell_i), profile(i), dt);
    integ_I = integ_I + dt*profile(i);
    voltage_soc(i) = max(min(polyval(polynomial_coeffs, v_o(i)), 1.0), 0.0);
    current_soc(i) = min(max(initial_v_soc - integ_I/charge_coulombs, 0.0), 1.0);
    alpha = piecewise_map(z_pos, w_pos, voltage_soc(i));%subs(blending_f, z, voltage_soc(i));
    weighted_soc(i) = alpha.*voltage_soc(i) +  (1-alpha).*current_soc(i);
end

figure()

figure()
hold on
plot(v_data(1:end-1, 1), voltage_soc.*100);
plot(v_data(1:end-1, 1), current_soc.*100);
plot(v_data(1:end-1, 1), weighted_soc.*100);
%plot(v_data(1:end,1), profile(1:end-1));
legend(["Voltage Estimation", "Current Estimation", "Weighted SoC"])
xlabel("Time (s)");
ylabel("State of Charge Estimation (%)");
title("State of Charge Estimation for Full 0.2C Discharge")
