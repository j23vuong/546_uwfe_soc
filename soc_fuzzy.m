% Setup
fis = sugfis("Name", "SoC");
fis = addInput(fis, [2.4 4.25], "Name", "voltage");
fis = addInput(fis, [0 64800], "Name", "integrated_current");

% Inputs
fis = addMF(fis,"voltage","trapmf",[2.4 2.4 3.4 3.6],"Name","low");
fis = addMF(fis,"voltage","trapmf",[3.4 3.6 3.8 4.0317],"Name","medium");
fis = addMF(fis,"voltage","trapmf",[3.7 4.0317 4.25 4.25],"Name","high");

max_charge = 64800;
fis = addMF(fis,"integrated_current","linzmf",[max_charge*0.2 max_charge*0.4],"Name","low");
fis = addMF(fis,"integrated_current","trapmf",[max_charge*0.2 max_charge*0.4 max_charge*0.6 max_charge*0.8],"Name","medium");
fis = addMF(fis,"integrated_current","linsmf",[max_charge*0.6 max_charge*0.8],"Name","high");

% Output
fis = addOutput(fis,[0 100],"Name","SoC");
fis = addMF(fis,"SoC","linear",[30 -0.00001 -71.352],"Name","low");
fis = addMF(fis,"SoC","linear",[40 -0.0010 -55.6],"Name","medium");
fis = addMF(fis,"SoC","linear",[50 -0.0005 -110],"Name","high");

% Rules
rule1 = "If voltage is low and integrated_current is high then SoC is low";
% rule2 = "If voltage is low then SoC is low";
rule3 = "If voltage is medium or integrated_current is medium then SoC is medium";
rule4 = "If voltage is high and integrated_current is low then SoC is high";
% rule5 = "If voltage is high then SoC is high";
rules = [rule1 rule3 rule4];
fis = addRule(fis,rules);

evalfis(fis, [4.2 0])
evalfis(fis, [3.3 32400])
evalfis(fis, [2.4 64800])


% Data reading
constant_discharge_file = "UWFEAdjVoltage.csv";
variable_discharge_file = "socVariableLoad.csv";
full_discharge_file = "fullSocDischarge.csv";
v_data = readmatrix(full_discharge_file);
raw_data = v_data;

initial_time_ms = v_data(1,1);
for i = 2:length(v_data(1:end,1))
    if v_data(i,1) < v_data(i-1,1)
        v_data(i:end,1) = v_data(i:end,1) - v_data(i,1) + v_data(i-1,1) + 75;
    end
end
% Time
v_data(1:end, 1) = (v_data(1:end, 1) - initial_time_ms) / 1000;
v_data(232117:end, :) = [];

% Calculations
% Arrays
v_o = zeros(1, size(v_data,1)-1);
current_soc = zeros(1, size(v_data,1)-1);
voltage_soc = zeros(1, size(v_data,1)-1);
weighted_soc = zeros(1, size(v_data,1)-1);
i_o = zeros(1, size(v_data,1)-1);
fuzzy_socs = zeros(1, size(v_data,1)-1);

steady = linspace(0,0, 1);
load = linspace(3.6,3.6,length(v_data(1:end,1)));
profile = cat(2, steady, load);
integ_I = 0;

% Constants
polynomial_coeffs = [1.76063275065311	-41.6171166467157	424.773203034036	-2441.25816393788	8622.40759658763	-19107.3233169624	25830.3060680661	-19345.5446537532	6077.85580095108];
initial_v_soc = min(max(polyval(polynomial_coeffs, v_data(1, cell_i)), 0.0), 1.0);
dt = v_data(2,1) - v_data(1,1);
charge_coulombs = 3*6*3600;
cell_i = 3;
cell_raw_current = 22;
cell_integrated_current = 24;
syms z
w_pos = [0.95 0.7 0.3 0.05];
z_pos = [0.75 0.25 0.75];

for i = 1:(size(v_data,1))-1
    % Calculations
    raw_current = v_data(i,cell_raw_current);
    % raw_current = profile(i);
    v_o(i) = calculate_output_voltage(v_data(i,cell_i), raw_current, dt);
    integ_I = v_data(i, cell_integrated_current);
    % integ_I = integ_I + dt*profile(i);
    i_o(i) = integ_I;
    current_soc(i) = min(max(initial_v_soc - integ_I/charge_coulombs, 0.0), 1.0);
    voltage_soc(i) = max(min(polyval(polynomial_coeffs, v_o(i)), 1.0), 0.0);
    % Fuzzy
    fuzzy_socs(i) = evalfis(fis, [v_o(i) integ_I]);
    % Naive
    alpha = piecewise_map(z_pos, w_pos, voltage_soc(i));%subs(blending_f, z, voltage_soc(i));
    weighted_soc(i) = alpha.*voltage_soc(i) +  (1-alpha).*current_soc(i);
end

% Plotting
% subplot(3,1,1)
% plotmf(fis,"input",1)
% subplot(3,1,2)
% plotmf(fis,"input",2)

% subplot(3,1,3)
plot(v_data(1:end-1, 1), fuzzy_socs)
hold on
plot(v_data(1:end-1, 1), weighted_soc .* 100, 'r')

