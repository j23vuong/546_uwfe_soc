y_data = [];
x_data = [];
w_pos = [0.95 0.7 0.3 0.05];
z_pos = [0.75 0.25 0.75];

for i = 1:1000
    x_data(i) = i/1000;
    y_data(i) = piecewise_map(z_pos, w_pos, x_data(i));
end
plot(x_data, y_data);
xlim([0 1]);
ylim([0 1]);
xlabel("Input SoC");
ylabel("\alpha");
title("Piecewise Weighting Map")