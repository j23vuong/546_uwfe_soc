function alpha = piecewise_map(y_coeffs, x_coeffs, x)
    if x >= x_coeffs(1)
        alpha = y_coeffs(1);
    elseif x <= x_coeffs(4)
        alpha = y_coeffs(3);
    elseif x_coeffs(1) >= x && x >= x_coeffs(2)
        alpha = (y_coeffs(2) - y_coeffs(1)) / (x_coeffs(2) - x_coeffs(1)) * (x - x_coeffs(1)) + y_coeffs(1);
    elseif x_coeffs(2) >= x && x >= x_coeffs(3)
        alpha = y_coeffs(2);
    elseif x_coeffs(3) >= x && x >= x_coeffs(4)
        alpha = (y_coeffs(3) - y_coeffs(2)) / (x_coeffs(4) - x_coeffs(3)) * (x - x_coeffs(3)) + y_coeffs(2);
    end
end