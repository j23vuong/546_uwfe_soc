clc;
clear;
close all;
data_10A = readmatrix('10ADischarge.csv');
data_20A = readmatrix('20ADischarge.csv');
data_20A = data_20A(1:end-2, 1:end);
data_30A = readmatrix('30ADischarge.csv');
data_30A = data_30A(1:end-2, 1:end);
discharge_data = {data_10A data_20A data_30A};
current = {10 20 30};

plot_voltage = 1;
plot_fit = 1;

smallest_dV = 0.02;
RC_constants = [];
R_ps = [];
dc_drops = [];
drop_idx = [];
bump_idxs = [];
for dis = 1:3
    %% Data Parsing
    curr_data = discharge_data{dis};
    initial_time_ms = curr_data(1,1);
    if plot_voltage == 1
        figure();
        plot((curr_data(1:end, 1) - initial_time_ms)/1000, curr_data(1:end, 2));
        xlabel("Time (s)");
        ylabel("Cell Voltage (V)");
        title("Continuous DC discharge at " + current{dis} + "A");
    end

    initial_voltage = mean(curr_data(1:100,2:end));
    
    %% Step Response Discharge (t = 0)
    % Vo(0) = Vi(0) + I(Ro + Rp)
    % Find sharp drop at discharge
    drop_idx = find(curr_data(1:end,2)<initial_voltage(1)-smallest_dV, 1, 'first');
    drop_idxs(dis) = drop_idx;
    % Vo - Vi, give some buffer in the subtraction to prevent sampling error
    dc_drops(dis, 1:20) = curr_data(drop_idx-2, 2:end) - curr_data(drop_idx+1, 2:end);

    %% Step Response, release discharge
    % Vo(t) = Vi(t) + I(Ro + Rp(1-e^{-t/(CRp)}))
    bump_idx = find(curr_data((drop_idx+5):end,2)>curr_data(drop_idx+1,2),1,'first')+drop_idx+4;
    bump_idxs(dis) = bump_idx;
    ft = fittype('A+B*(1-exp(-t/(B*C)))','indep','t');
    time_0_ms = curr_data(bump_idx+2,1);
    step_models = {};
    metrics = {};
    for cell = 2:21
        x_data = (curr_data(bump_idx+2:end,1)-time_0_ms)/1000;
        y_data = curr_data(bump_idx+2:end, cell);
        [mdl, gof] = fit(x_data, y_data, ft, 'Start', [3.6 0.2 1e1], 'Lower', [1e-14, 1e-14, 1e-14]);
        step_models{cell-1} = mdl;
        metrics{cell-1} = gof;
        if plot_fit == 1 & cell == 2
            figure()
            plot(step_models{1}, x_data, y_data);
            xlabel("Time (s)");
            ylabel("Cell Voltage (V)");
        end
        coeffs = coeffvalues(step_models{cell-1});
        R_ps(dis, cell-1) = coeffs(2);
        RC_constants(dis, cell-1) = coeffs(2)*coeffs(3);
    end
end
% Need to solve the system of equations
% dc_drops = I(Ro + Rp)
% RC_constants = Rp*C
I = cell2mat(current);
R_p = mean(mean(R_ps,2)./I');
R_o = mean(mean(dc_drops,2)./I');
C = mean(RC_constants, 'all')./R_p;
