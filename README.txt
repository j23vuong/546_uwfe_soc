Description of Files:
10ADischarge.csv: test data from a constant 10A discharge used for system identification of the battery model
20ADischarge.csv: test data from a constant 20A discharge used for system identification of the battery model
30ADischarge.csv: test data from a constant 30A discharge used for system identification of the battery model
calculate_output_voltage.m: is a matlab script that takes in measured cell voltages and outputs the battery model voltages
fit_stuff.m: Script used for testing of battery model and verification
fullSocDischarge.csv: Test Data Set
IZ_Calculation.m: Used for battery model system identification
IZ_sims.m: Verification of battery model
mte546_project_ekf_demo.m: Extended Kalman Filter Code used for analysis
piecewise_map.m: Used for naive weighting fusion method
plot_piecewise.m: Plotting script for report
slopeFusion.m: Weighted slope fusion method script
soc_fuzzy.m: Script used for fuzzy logic fusion method
soc_sim.m: Initial verification and analysis of training data
UWFEAdjVoltage.csv: Training Data Set
voltage_to_soc.m: Polynomial used to fit the discharge to voltage curve
