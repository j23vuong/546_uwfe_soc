v_data = readmatrix('UWFEAdjVoltage.csv');
raw_data = v_data;

plot_raw = 0;
plot_adjusted = 1;

for i = 2:length(v_data(1:end,1))
    if v_data(i,1) < v_data(i-1,1)
        v_data(i:end,1) = v_data(i:end,1) - v_data(i,1) + v_data(i-1,1) + 75;
    end
end
v_data(1:end, 1) = (v_data(1:end, 1) - initial_time_ms) / 1000;
v_data(232117:end, :) = [];
if plot_raw == 1
    figure()
    plot(v_data(1:end, 1), v_data(1:end, 2))
end

integ_VC = 0;
integ_I = 0;
v_o = [];
profile = [];
steady = linspace(0,0, 1);
load = linspace(3.6,3.6,length(v_data(1:end,1)));
profile = cat(2, steady, load);


for i = 1:(size(v_data,1))-1
    v_1 = profile(i) .* R_o;
    v_2 = (1/C) .* (integ_I - 1/R_p.*integ_VC);
    dt = v_data(i+1,1) - v_data(i,1);
    integ_VC = integ_VC + v_2 * dt;
    integ_I = integ_I + profile(i) * dt;
    v_o(i) = v_data(i, 2) + v_1 + v_2;
end
v_o = v_o';
if plot_adjusted == 1
    figure()
    hold on
    plot(v_data(1:end-1,1), v_o);
    plot(v_data(1:end, 1), v_data(1:end, 2))
end
hold on;
soc_curve = linspace(1.0, 0.0, length(v_data(1:end, 1)))';
%f = fit(v_data(1:end, 2), soc_curve, 'poly7');
f = polyfit(v_data(1:end, 2), soc_curve, 8);
%plot(f, v_data(1:end, 2), soc_curve);

plot(v_data(1:end, 2), soc_curve);
plot(v_data(1:end, 2), polyval(f, v_data(1:end, 2)));

figure()
plot(v_data(1:end, 1), polyval(f, v_data(1:end, 2)));


% 6077.86-19345.545x+25830.306x^{2}-19107.323x^{3}+8622.4076x^{4}-2441.25816x^{5}+424.773203x^{6}-41.61711664x^{7}+1.7606327x^{8}
% https://www.desmos.com/calculator/cqr4r1aw3e
