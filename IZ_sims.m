clc;
clear;
close all;

R_p = 0.0015;
R_o = 0.0031;
C = 9.6463e3;

trial = 3;
data_10A = readmatrix('10ADischarge.csv');
data_20A = readmatrix('20ADischarge.csv');
data_30A = readmatrix('30ADischarge.csv');
datas = {data_10A data_20A data_30A};


bump_idxs = [1102 1453 1106];
drop_idxs = [250 253 152];
currents = [10 20 30];
lengths = [2070 2194 3076];

current_profiles = [];
for i = 1:3
    profile = [];
    steady = linspace(0,0, drop_idxs(i)-1);
    load = linspace(currents(i), currents(i), bump_idxs(i)-drop_idxs(i));
    unload = linspace(0, 0, lengths(i)-bump_idxs(i));
    profile = cat(2, steady, load, unload);
    current_profiles(i, 1:length(profile)) = profile;
end

profile = current_profiles(trial, 1:end);
data = datas{trial};

v_i = data(1, 2);
integ_I = 0;
integ_VC = 0;
v_s = [];
v_o = [];
for i = 1:(size(data,1))-1
    v_1 = profile(i) .* R_o;
    v_2 = (1/C) .* (integ_I - 1/R_p.*integ_VC);
    dt = (data(i+1,1) - data(i,1))/1000;
    integ_VC = integ_VC + v_2 * dt;
    integ_I = integ_I + profile(i) * dt;
    v_s(1, i) = v_1;
    v_s(2, i) = v_2;
    v_o(i) = v_i - v_1 - v_2;
end
x_data = ((data(1:end-1,1) - data(1,1))/1000)';
figure()
hold on;
%plot(x_data, v_o);
if 0
    title("Continuous Discharge at 10A");
    yyaxis left;
    plot(x_data, data(1:end-1,2));
    ylabel("Cell Voltage (V)");
    yyaxis right;
    plot(x_data, profile(1:length(data(1:end-1,2))));
    ylim([-1 15])
    ylabel("Current (A)");
    xlabel("Time (s)");
    xlim([0 80])
end
%plot(x_data, v_s(1, 1:end));
%plot(x_data, v_s(2, 1:end));
%plot(x_data, profile(1:length(x_data)));


integ_VC = 0;
integ_I = 0;
v_o2 = [];
for i = 1:(size(data,1))-1
    if 0
        v_1 = profile(i) .* R_o;
        v_2 = (1/C) .* (integ_I - 1/R_p.*integ_VC);
        dt = (data(i+1,1) - data(i,1))/1000;
        integ_VC = integ_VC + v_2 * dt;
        integ_I = integ_I + profile(i) * dt;
        v_o2(i) = data(i, 2) + v_1 + v_2;
    end
    dt = (data(i+1,1) - data(i,1))/1000;
    v_o2(i) = calculate_output_voltage(data(i,2), profile(i), dt);
end

figure()

hold on
plot(x_data, v_o2);
plot(x_data, data(1:end-1, 2));
xlabel("Time (s)")
ylabel("Cell Voltage (V)")
legend(["Adjusted Cell Voltage", "Measured Cell Voltage"]);
title("Continuous discharge at 30A");








